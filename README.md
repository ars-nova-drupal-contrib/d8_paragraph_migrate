# d8_paragraph_migrate

Drupal 8 site created to demonstrate migrating paragraph content from D7 to D8.

Works in conjunction with [d7_paragraph_migrate site](https://gitlab.com/ars-nova-public/d7_paragraph_migrate).

Included is a bare-bones Drupal 8 site with:

- drupal/migrate_plus
- drupal/migrate_tools
- drupal/migrate_upgrade
- drupal/paragraphs
- drush/drush
- initial database that populates automatically on startup

## Requirements

Uses [docker4drupal](https://github.com/wodby/docker4drupal).

Requires:

- docker
- docker-compose

## Usage

##### Prep

- add `127.0.0.1 d8.paragraph-migrate.local` to /etc/hosts

##### Directory structure

```
d7_d8_paragraphs/
|
|-- d7_paragraph_migrate/     (d7 site)
|
|-- d8_paragraph_migrate/     (d8 site)
|
\-- volumes/                  (created automatically by docker)
    |-- mariadb/
    \-- mariadb_d8/
```

##### Install

```
# if parent directory doesn't exist
mkdir d7_d8_paragraphs;

cd d7_d8_paragraphs;
git clone git@gitlab.com:ars-nova-public/d8_paragraph_migrate.git
cd d8_paragraph_migrate;
git submodule init;
git submodule update;
make up;

make shell;
cd drupal;
composer install;

exit;
```

Log in at [http://d8.paragraph-migrate.local/](http://d8.paragraph-migrate.local/) to confirm site is running and there's no content, content types, or paragraph bundles.
Credentials: admin/admin.

##### Migrate

- In d8_paragraph_migrate directory

```
make shell;
cd drupal;

drush en paragraphs field_ui image comment taxonomy;

drush migrate:upgrade --legacy-db-key=migrate --legacy-root=http://d7_paragraph_migrate_apache --configure-only;

drush mim upgrade_d7_image_settings,upgrade_d7_image_styles,upgrade_d7_filter_format;

drush mim upgrade_d7_node_type,upgrade_d7_paragraphs_type;

drush mim upgrade_d7_comment_type,upgrade_d7_comment_field,upgrade_d7_comment_field_instance,upgrade_d7_comment_entity_display,upgrade_d7_comment_entity_form_display,upgrade_d7_comment_entity_form_display_subject;

drush mim upgrade_d7_field,upgrade_d7_view_modes,upgrade_d7_field_instance,upgrade_d7_view_modes,upgrade_d7_field_formatter_settings,upgrade_d7_field_instance_widget_settings;

drush mim upgrade_d7_filter_settings,upgrade_d7_image_settings,upgrade_d7_node_settings,upgrade_text_settings,upgrade_d7_filter_format;

drush mim upgrade_d7_node_title_label

drush mim upgrade_file_settings,upgrade_d7_file;

drush en d7_product_migrate;

drush mim d7_product_paragraph_description,d7_product_paragraph_resources,d7_node_product;

exit;

sudo chown -RH 82:82 ./drupal/web/sites/default/files;
make drush cr;
```
